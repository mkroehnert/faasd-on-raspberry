'use strict'

const fs = require('fs')
const fsPromises = fs.promises

module.exports = async (event, context) => {
  let dir = event.body || '/'

  let dir_content = await fsPromises
    .readdir(dir)
  console.log(dir, dir_content)
  return context
    .status(200)
    .succeed(JSON.stringify(dir_content))
}

