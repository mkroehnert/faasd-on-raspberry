taken from https://www.openfaas.com/blog/faasd-tls-terraform/

Generate token
`echo $(head -c 16 /dev/urandom |shasum |cut -d "-" -f1) > token.txt`

Store token on faasd instance
`faas-cli secret create private-api-token --from-file token.txt`

Adapt `private-api` deployment file `private-api.yml`:
```
functions:
  private-api:
    ...
    secrets:
    - private-api-token
```

Unauthorized access

`curl -i ${OPENFAAS_URL}/function/private-api`

Authorized access

```curl -i -H "Authorization: Bearer: $(cat token.txt)" \
  ${OPENFAAS_URL}/function/private-api```
