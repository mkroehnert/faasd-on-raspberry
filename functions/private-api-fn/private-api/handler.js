'use strict'

const fs = require('fs')
const fsPromises = fs.promises

module.exports = async (event, context) => {
  let secret = await fsPromises
    .readFile('/var/openfaas/secrets/private-api-token', 'utf8')

  let auth = event.headers['authorization']
  if(auth && auth == 'Bearer: ' + secret) {
    return context
      .status(200)
      .succeed('authorized')
  }

  return context
    .status(401)
    .succeed('unauthorized')
}

