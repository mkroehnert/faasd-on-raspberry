# Installation on Raspberry Pi4B

Instructions: https://blog.alexellis.io/faasd-for-lightweight-serverless/

However:
* only works with 32 Bit RasperryOs
* containerd does not build easily on 64 Bit RaspberryOS beta version

Local Nixos setup (faas-cli + helper scripts)
* `git clone https://gitlab.com/mkroehnert/faasd-on-raspberry.git`
* `cd faasd-on-raspberry`
* adapt variables in shell.nix (the ones in the block `custom variables` directly after `let`)
* `nix-shell ./shell.nix`

faasd web-ui
* URL: http://raspberry.local:8080/ui/ (or open with `faasd-webui`)
* username: admin
* password: /var/lib/faasd/secrets/basic-auth-password (get it via `faasd-get-password` in `nix-shell`)


# Docker Registry for deploying functions

Use GitLab instead of Docker hub.

First get a GitLab API- or deploy-key.

Then `docker login registry.gitlab.com`
* username: $gitlab-username
* password: $gitlab-api-key

Set OPENFAAS_PREFIX to `registry.gitlab.com/<gitlab-username>/<project_name>/` before creating new functions with `faas-cli` (or set `of_docker_registry_prefix` when using `shell.nix`).

Use private Docker registry with faasd:
*  `scp ~/.docker/config.json $user@$pi:/var/run/faasd/.docker/config.json`


# Get additional function templates

* https://github.com/openfaas/faas-cli/blob/master/guide/TEMPLATE.md


# Get URL for triggering function

* UI -> click on function
* `faas-cli describe <function-name>`


# Port forwarding with tunnelto.dev

* `git clone https://github.com/agrinman/tunnelto.git`
* `cd tunnelto`
* `docker run --rm -it -v /home/mk/projects/raspberrypi/tunnelto/:/home/rust/src messense/rust-musl-cross:armv7-musleabihf cargo build --release --target=armv7-unknown-linux-musleabihf`
* `scp target/armv7-unknown-linux-musleabihf/release/tunnelto pi@pi.local:/home/pi/bin`

Go to https://tunnelto.dev to get an API token.
Afterwards store the API token on the PI with `tunnelto set-auth` or copy existing configuration file to the PI.

* activate tunnel: `/home/pi/bin/tunnelto --port 8080`
* visit generated domain or pass in `--subdomain` for dedicated domain name

Cross-compiling solutions from Linux-x64 to Linux-armhf
* Cross did not work:
  * https://github.com/rust-embedded/cross
  * https://github.com/rust-embedded/cross/issues/260
  * https://github.com/rust-embedded/cross/issues/437
  * https://github.com/rust-embedded/cross/issues/260
* rust-musl-builder did not work:
  * https://github.com/emk/rust-musl-builder
  * https://github.com/emk/rust-musl-builder/issues/15
* rust-musl-cross worked:
  * https://github.com/messense/rust-musl-cross


# Create function for running on ARM faasd

NOTE: so, Docker containers seem to be tied to the architecture they were created on. Docker multi-arch templates must be used to cross-compile from x64 to armv7.

Open `nix-shell` before creating a new function

* `mkdir echo-fn; cd echo-fn`
* `faas-cli template pull https://github.com/alexellis/multiarch-templates`
* `faas-cli new echo --lang node12-multiarch`

Setup docker buildx (if not already installed)
* `docker-buildx`: https://github.com/docker/buildx/
* `buildx` docs: https://docs.docker.com/buildx/working-with-buildx/
* export `DOCKER_CLI_EXPERIMENTAL=enabled` to enable (setup by default when using `shell.nix`)

Setup multiarch/qemu-static
* `qemu-user-static`: https://github.com/multiarch/qemu-user-static
* required as found in this ticket: https://github.com/docker/buildx/issues/138
* install with `docker run --rm --privileged multiarch/qemu-user-static --reset -p yes`

Build multiarch function (requires faas-cli > 0.12.19)
* manual
  * `docker buildx create --use --name=multiarch --node=multiarch`
  * `faas-cli build --shrinkwrap -f echo.yml`
  * `faas-cli publish --yaml echo.yml --image $OPENFAAS_PREFIX/echo:latest --platforms linux/amd64,linux/arm/v7,linux/arm64 --name echo`
  * `faas-cli deploy --image $OPENFAAS_PREFIX/echo:latest --name echo`
* `shell.nix`
  * `faas-cli-up-multiarch echo`


# Using secrets

Check [private-api](functions/private-api-fn/readme.md)


# Debugging

`faas-cli logs private-api`


# Details on installation

`/var/lib/faasd`
* `hosts` contains IP addresses of containers run by faasd
* `docker-compose.yml`
    -> Prometheus: export ports to 127.0.0.1 or 0.0.0.0
    -> Gateway: change default 8080 port to something else

`/var/lib/faasd-provider`
* `secrets/*` -> contains secrets stored via `faas-cli` -> mounted as `/var/openfaas/secrets/`

# Where are faas-cli login credentials stored locally?

default: `~/openfaas/config.yml`

# How to restart faasd
`sudo systemctl restart faasd faasd-provider`
