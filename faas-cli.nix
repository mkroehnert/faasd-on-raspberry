{
  stdenv,
  fetchurl
}:

stdenv.mkDerivation rec {
  name = "faas-cli-${version}";
  version = "0.12.19";

  src = fetchurl {
    url = "https://github.com/openfaas/faas-cli/releases/download/${version}/faas-cli";
    sha256 = "4798f7d391508df970b32ea4a7f3ec0c09c820ffcc8317853402577eab5247ec";
  };

  sourceRoot = ".";
  unpackCmd = "cp $curSrc .";

  dontPatch = true;
  dontConfigure = true;
  dontBuild = true;
  dontFixup = true;

  installPhase = ''
    mkdir -p $out/bin
    mv ./*faas-cli $out/bin/faas-cli
    chmod a+x $out/bin/faas-cli
  '';

  meta = with stdenv.lib; {
    homepage = "https://github.com/openfaas/faas-cli";
    description = "OpenFAAS commandline tool";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = [ "It's a me" ];
  };
}
