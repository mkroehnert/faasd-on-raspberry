{
  stdenv,
  fetchurl
}:

stdenv.mkDerivation rec {
  name = "docker-buildx-${version}";
  version = "0.4.2";

  src = fetchurl {
    url = "https://github.com/docker/buildx/releases/download/v${version}/buildx-v${version}.linux-amd64";
    sha256 = "16gqzllpwzgcvq71wy5fjjz8slazx99856dps6jlyfp9dlshf7y2";
  };

  sourceRoot = ".";
  unpackCmd = "cp $curSrc .";

  dontPatch = true;
  dontConfigure = true;
  dontBuild = true;
  dontFixup = true;

  installPhase = ''
    mkdir -p $out/usr/local/lib/docker/cli-plugins
    chmod a+x ./*buildx-v*
    mv ./*buildx-v* $out/usr/local/lib/docker/cli-plugins/docker-buildx
  '';

  meta = with stdenv.lib; {
    homepage = "https://github.com/docker/buildx";
    description = "docker buildx: use BuildKit for building containers";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = [ "It's a me" ];
  };
}
