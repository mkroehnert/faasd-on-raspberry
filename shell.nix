# run with nix-shell .

{
  pkgs ? import <nixpkgs> {}
}:

let
  # packages
  faas-cli = pkgs.callPackage ./faas-cli.nix { };
  docker-buildx = pkgs.callPackage ./docker-buildx.nix { };

  # custom variables
  of_ip = "192.168.0.2";
  of_user = "pi";
  of_gateway_url = "http://" + of_ip + ":8080";
  of_docker_registry_prefix = "registry.gitlab.com/mkroehnert/faasd-on-raspberry";
  of_config_dir = builtins.toString ./. + "/.openfaas";
  of_log_max_lines = "40";
  docker_cli_plugins_dir = "$HOME/.docker/cli-plugins";
  docker_build_architectures = "linux/amd64,linux/arm/v7,linux/arm64";
  ssh = "${pkgs.openssh}/bin/ssh";

  # custom shell scripts
  faas-cli-login = pkgs.writeScriptBin "faas-cli-login" ''
    #!${pkgs.stdenv.shell}
    cat ${of_config_dir}/faas-passwd.txt | faas-cli login --gateway ${of_gateway_url} --password-stdin
  '';
  faasd-webui = pkgs.writeScriptBin "faasd-webui" ''
    #!${pkgs.stdenv.shell}
    xdg-open ${of_gateway_url}/ui/
  '';
  faasd-get-password = pkgs.writeScriptBin "faasd-get-password" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- sudo cat /var/lib/faasd/secrets/basic-auth-password > ${of_config_dir}/faas-passwd.txt
  '';
  faasd-restart = pkgs.writeScriptBin "faasd-restart" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- "sudo systemctl restart faasd faasd-provider
  '';
  faasd-shutdown = pkgs.writeScriptBin "faasd-shutdown" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- sudo shutdown -h now
  '';
  faasd-get-logs = pkgs.writeScriptBin "faasd-get-logs" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- "sudo systemctl status faasd; echo ""; sudo systemctl status faasd-provider" | less
  '';
  faasd-get-logs-daemon = pkgs.writeScriptBin "faasd-get-logs-daemon" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- "journalctl -u faasd --lines ${of_log_max_lines}; echo ""; journalctl -u faasd-provider --lines ${of_log_max_lines}" | less
  '';
  faasd-get-logs-core = pkgs.writeScriptBin "faasd-get-logs-core" ''
    #!${pkgs.stdenv.shell}
    ${ssh} ${of_user}@${of_ip} -- "journalctl -t default:gateway --lines ${of_log_max_lines}; echo ""; journalctl -t default:nats --lines ${of_log_max_lines}; echo ""; journalctl -t default:queue-worker --lines ${of_log_max_lines}; echo ""; journalctl -t default:prometheus --lines ${of_log_max_lines}" | less
  '';
  faas-cli-deploy = pkgs.writeScriptBin "faas-cli-deploy" ''
    #!${pkgs.stdenv.shell}
    echo Deploying $1
    faas-cli deploy --image $OPENFAAS_PREFIX/$1:latest --name $1
  '';
  faas-cli-publish-multi = pkgs.writeScriptBin "faas-cli-publish-multi" ''
    #!${pkgs.stdenv.shell}
    echo Building $1
    faas-cli publish --yaml $1.yml --image $OPENFAAS_PREFIX/$1:latest --platforms ${docker_build_architectures} --name $1
  '';
  faas-cli-up-multi = pkgs.writeScriptBin "faas-cli-up-multi" ''
    #!${pkgs.stdenv.shell}
    faas-cli-publish-multi $1
    faas-cli-deploy $1
  '';
  docker-setup-qemu-static = pkgs.writeScriptBin "docker-setup-qemu-static" ''
    #!${pkgs.stdenv.shell}
    ${pkgs.docker}/bin/docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
  '';
in pkgs.mkShell {
  buildInputs = [
    pkgs.openssh
    pkgs.docker
    pkgs.buildkit
    pkgs.git

    docker-buildx
    faas-cli

    faas-cli-login
    faasd-webui
    faasd-get-password
    faasd-restart
    faasd-shutdown
    faasd-get-logs
    faasd-get-logs-daemon
    faasd-get-logs-core
    faas-cli-deploy
    faas-cli-publish-multi
    faas-cli-up-multi
    docker-setup-qemu-static
  ];

  shellHook = ''
    echo OpenFAAS
    echo "  URL   : $OPENFAAS_URL"
    echo "  PREFIX: $OPENFAAS_PREFIX"
    echo "  CONFIG: $OPENFAAS_CONFIG"

    mkdir -p $OPENFAAS_CONFIG

    echo "Setting up docker qemu-static"
    docker-setup-qemu-static

    echo "Setting up docker-buildx"
    mkdir -p ${docker_cli_plugins_dir}
    ln --symbolic --force ${docker-buildx}/usr/local/lib/docker/cli-plugins/docker-buildx ${docker_cli_plugins_dir}/docker-buildx
    docker buildx create --use --name=multiarch --node=multiarch
  '';

  # faas-cli overrides
  # see https://github.com/openfaas/faas-cli
  OPENFAAS_PREFIX = of_docker_registry_prefix;
  OPENFAAS_URL = of_gateway_url;
  #  OPENFAAS_TEMPLATE_URL = "";
  OPENFAAS_CONFIG = of_config_dir;

  # Docker variables
  DOCKER_BUILDKIT = 1;
  DOCKER_CLI_EXPERIMENTAL = "enabled";
}
